# Contributing

### Repo Rules
- Please use Pull Requests when committing to a public brb anch
- Adam Otto will is set as a default reviewer on all PRs

### What else you might see in a CONTRIBUTING.md file
- test coverage rules
- linting rules
- Commit message formating
