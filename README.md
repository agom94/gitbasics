# README


### What

- This project is meant to help the MOCyber team learn how to use git.

### Getting Started

- All you will need is a text editor of your choice. We will be using VScode.

### Contact

- Contact Adam Otto (ottomada@gmail.com) for any questions.